ansible-role-configuration
=========

This role serves as a configuration templating/configuration in general role for myself. It is barebones and very opinionated, thus I can't recommend you to use it, even though you could.

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

    - hosts: localhost
      roles:
         - role: ansible-role-configuration

License
-------

BSD
